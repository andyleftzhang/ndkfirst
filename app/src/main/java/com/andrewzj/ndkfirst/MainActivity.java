package com.andrewzj.ndkfirst;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView tv;

    JniNative jniNative;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Example of a call to a native method
        tv = (TextView) findViewById(R.id.sample_text);

        jniNative = new JniNative();

         if (jniNative.callJavaInstanceField() == 0){
            tv.setText(jniNative.stringFromJNI()+jniNative.getAnInt());
         }

         if (jniNative.callJavaStaticField() == 0){
             tv.setText(JniNative.anStr + jniNative.getAnInt());
         }

        tv.postDelayed(new Runnable() {
            @Override
            public void run() {
                jniNative.callJavaInstanceMethod();
            }
        },1000);

        tv.postDelayed(new Runnable() {
            @Override
            public void run() {
                jniNative.callJavaStaticMethod();
            }
        },2000);


        int res = jniNative.dosomething(9,100);
        Log.e("NDK test:",res+"");
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


}
