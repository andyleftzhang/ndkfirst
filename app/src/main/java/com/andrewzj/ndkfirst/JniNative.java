package com.andrewzj.ndkfirst;

import android.util.Log;

/**
 * @author zhangjie17
 * @weibo_version
 * @date 2018/9/25
 */
public class JniNative {

    static {
        System.loadLibrary("native-lib");
    }

    private int anInt = 100;
    public static String  anStr= "hello";

    public int getAnInt() {
        return anInt;
    }

    public void setAnInt(int anInt) {
        this.anInt = anInt;
    }

    public native String stringFromJNI();

    //测试代码
    public  native void callJavaInstanceMethod();
    //等待调用的实例方法
    public void callInstanceMethod(){
        Log.e("NDK test: ", "callInstanceMethod");
    }

    //测试代码
    public native void callJavaStaticMethod();
    //等待调用的静态方法
    public static void callStaticMethod(){
        Log.e("NDK test: ", "callStaticMethod");
    }

    //测试代码
    public native int callJavaInstanceField();

    //测试代码
    public native int callJavaStaticField();


    public native int dosomething(int a, int b);

}
