#include <jni.h>
#include <string>
#include "Native.h"

extern "C" JNIEXPORT jstring

JNICALL
Java_com_andrewzj_ndkfirst_JniNative_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}


extern "C"
JNIEXPORT void JNICALL
Java_com_andrewzj_ndkfirst_JniNative_callJavaInstanceMethod(JNIEnv *env, jobject instance) {

    jclass clazz = NULL;
    jmethodID mid_instance = NULL;

    //1. 找到该类对象
    clazz = env->FindClass("com/andrewzj/ndkfirst/JniNative");
    if (clazz == NULL){
        return;
    }

    //2. 根据类对象查找需要回调的类方法
    mid_instance = env->GetMethodID(clazz,"callInstanceMethod","()V");
    if (mid_instance == NULL){
        return;
    }

    //3. 使用类实例对象调用该类方法
    env->CallVoidMethod(instance,mid_instance);

    //4. 释放资源
    env->DeleteLocalRef(clazz);
    env->DeleteLocalRef(instance);
}

extern "C"
JNIEXPORT void JNICALL
Java_com_andrewzj_ndkfirst_JniNative_callJavaStaticMethod(JNIEnv *env, jobject instance) {

    jclass clazz = NULL;
    jmethodID mid_static_method;
    //1. 查找类对象
    clazz = env->FindClass("com/andrewzj/ndkfirst/JniNative");
    if (clazz == NULL){
        return;
    }
    //2. 查找类对象静态方法
    mid_static_method = env->GetStaticMethodID(clazz,"callStaticMethod","()V");
    if (mid_static_method == NULL){
        return;
    }
    //3. 调用类静态方法
    env->CallStaticVoidMethod(clazz,mid_static_method);
    //4. 释放资源
    env->DeleteLocalRef(clazz);
    env->DeleteLocalRef(instance);
}

extern "C"
JNIEXPORT jint JNICALL
Java_com_andrewzj_ndkfirst_JniNative_callJavaInstanceField(JNIEnv *env, jobject instance) {

    jclass clazz = NULL;
    jfieldID  jfid = NULL;
    clazz = env->FindClass("com/andrewzj/ndkfirst/JniNative");
    if (clazz == NULL){
        return -1;
    }
    jfid = env->GetFieldID(clazz,"anInt","I");

    if (jfid == NULL){
        return -1;
    }

    env->SetIntField(instance,jfid,10086);

    env->DeleteLocalRef(instance);
    env->DeleteLocalRef(clazz);
    return 0;

}
extern "C"
JNIEXPORT jint JNICALL
Java_com_andrewzj_ndkfirst_JniNative_callJavaStaticField(JNIEnv *env, jobject instance) {

    jclass clazz = NULL;
    jfieldID jfid = NULL;
    jstring new_str = NULL;
    clazz = env->FindClass("com/andrewzj/ndkfirst/JniNative");
    if (clazz == NULL){
        return -1;
    }

    jfid = env->GetStaticFieldID(clazz,"anStr","Ljava/lang/String;");
    if (jfid == NULL){
        return -1;
    }

    new_str = env->NewStringUTF("Hello World");

    env->SetStaticObjectField(clazz,jfid,new_str);

    env->DeleteLocalRef(clazz);
    env->DeleteLocalRef(instance);

    return 0;
}

extern "C"
JNIEXPORT jint JNICALL
Java_com_andrewzj_ndkfirst_JniNative_dosomething(JNIEnv *env, jobject instance, jint a, jint b) {

    ClassA *ca = new ClassA();
    int res = ca->doSomethind(a,b);

    return res;
}